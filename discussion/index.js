console.log('hello');


// {

// "city": "Quezon City",
// "province":"Metro Manila",
// "country":"Philippines"

// }

let myArr = [
	{name: "Jino"},
	{name:"John"}
]


// {
// 	"cities":[
// 	{"city": "Quezon City"},
// 	{"city": "Makati City"}
// 	]
// }


let batchesArr = [
	{batchname: 'batch 145'},
	{batchname: 'batch 146'},
	{batchname: 'batch 147'}
]


'[{"batchname":"batch 145"},{"batchname":"batch 146"},{"batchname":"batch 147"}]'


let stringifiedData = JSON.stringify(batchesArr)
console.log(stringifiedData)


let fixedData = JSON.parse(stringifiedData)
console.log(fixedData)